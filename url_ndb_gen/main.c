#include <stdio.h>
#include <stdlib.h>

char *sig_gen_url(const char *str, const char *id) {
  char hex[256];
  char *sig = (char *)malloc(512);

  for (int i = 0; str[i] != '\0'; i++) {
    if (str[i] == '\n' || str[i] == '\r')
      break;

    snprintf(hex + i * 2, 256, "%02x", str[i]);
  }

  snprintf(sig, 512, "%s:7:*:%s\n", id, hex);

  return sig;
}

int main(int argc, char **argv) {
  FILE *in_f, *out_f;
  char *input_path, *output_path, *id, *sig;
  char *str = NULL;
  size_t str_size = 0;
  ssize_t line_size = 1;

  if (argc != 4) {
    printf("Usage:\nmain [blacklist.txt] [blacklist.ndb] [Detection ID]\n");
    return 1;
  }

  input_path = argv[1];
  output_path = argv[2];
  id = argv[3];

  // printf("Args: %s %s %s %s\n", argv[0], argv[1], argv[2], argv[3]);

  in_f = fopen(input_path, "r");
  if (!in_f) {
    printf("Failed to open file: %s\n", input_path);
    return -1;
  }

  out_f = fopen(output_path, "w+");
  if (!out_f) {
    printf("Failed to open file: %s\n", output_path);
    return -1;
  }

  while (!feof(in_f)) {
    line_size = getline(&str, &str_size, in_f);
    sig = sig_gen_url(str, id);
    printf("Sig: %s", sig);
    fputs(sig, out_f);
    free(sig);
  }

  fclose(in_f);
  fclose(out_f);
}