#pragma once

#include <stdio.h>
#include <string.h>

#include <orca/discord.h>

void cmd_sig_sha256(struct discord *client, const struct discord_message *msg);
void cmd_clam_reload(struct discord *client, const struct discord_message *msg);
void cmd_sig_url(struct discord *client, const struct discord_message *msg);