#pragma once

#include <stdio.h>

#include <orca/discord.h>

int auth(unsigned int id);

void get_attachment(const struct discord_message *msg, char *url,
                    char *filename);

FILE *get_file(const char *url, const char *filename, unsigned int id,
               char *path, unsigned int *size);

FILE *write_msg_file(unsigned int id, const char *str, char *path);