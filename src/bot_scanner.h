#pragma once

typedef struct _scanner_ctx_t {
  int fd;
  int id;
  char filename[256];
  char url[256];
} scanner_ctx_t;

typedef struct _scan_result_t {
  int result;
  char virname[64];
} scan_result_t;

void setup_clam_ctx();
void cleanup_clam_ctx();
int count_clam_sigs();
void *scan(void *ptr);
scan_result_t *scan_msg(unsigned int id, const char *msg);