#include "bot_sigtool.h"

#include <clamav.h>

#include <unistd.h>

#define FILEBUFF 8192

void sig_add(const char *sig, const char *db) {
  FILE *f;
  char path[256];

  snprintf(path, 256, "%s/%s", cl_retdbdir(), db);
  if (access(path, F_OK) == 0)
    f = fopen(path, "a+");
  else
    f = fopen(path, "w+");

  if (!f) {
    return;
  }

  fputs(sig, f);
  fclose(f);
}

char *sig_gen_sha256(FILE *f) {
  unsigned char digest[32], buffer[FILEBUFF];
  unsigned int bytes, i;
  char *sha;

  void *ctx = cl_hash_init("sha256");
  if (!ctx)
    return NULL;

  fseek(f, 0, SEEK_SET);

  while (bytes = fread(buffer, 1, sizeof(buffer), f)) {
    cl_update_hash(ctx, buffer, bytes);
  }
  fseek(f, 0, SEEK_SET);
  cl_finish_hash(ctx, digest);

  sha = (char *)malloc(65); // 64 characters + NULL terminator
  if (!sha)
    return NULL;

  for (i = 0; i < 32; i++)
    snprintf(sha + i * 2, 64, "%02x", digest[i]);

  return sha;
}

char *sig_gen_url(const char *str) {
  char hex[64];
  char *sig = (char *)malloc(256);

  for (int i = 0; str[i] != '\0'; i++)
    snprintf(hex + i * 2, 64, "%02x", str[i]);

  snprintf(sig, 256, "Html.Phishing.Discord:7:*:%s\n", hex);

  return sig;
}