#include "bot_clamav.h"

#define MAX_FILESIZE 1024 * 1024 * 50 // 50MB

unsigned int sigs = 0;

void clamav_init() { cl_init(CL_INIT_DEFAULT); }

clamav_ctx *clamav_ctx_init() {
  clamav_ctx *ctx = malloc(sizeof(clamav_ctx));
  ctx->engine = cl_engine_new();
  ctx->sigs = 0;

  cl_load(cl_retdbdir(), ctx->engine, &sigs, CL_DB_STDOPT | CL_DB_PUA);
  cl_engine_compile(ctx->engine);
  cl_engine_set_num(ctx->engine, CL_ENGINE_MAX_FILESIZE, MAX_FILESIZE);

  return ctx;
}

void clamav_ctx_free(clamav_ctx *ctx) {
  cl_engine_free(ctx->engine);
  free(ctx);
}

cl_error_t clamav_scan(clamav_ctx *ctx, int fd, const char **virname) {
  struct cl_scan_options options = {
      .general = CL_SCAN_GENERAL_ALLMATCHES | CL_SCAN_GENERAL_HEURISTICS,
      .parse = ~0,
      .heuristic = CL_SCAN_GENERAL_HEURISTIC_PRECEDENCE |
                   CL_SCAN_HEURISTIC_STRUCTURED |
                   CL_SCAN_HEURISTIC_BROKEN_MEDIA,
  };

  return cl_scandesc(fd, NULL, virname, NULL, ctx->engine, &options);
}