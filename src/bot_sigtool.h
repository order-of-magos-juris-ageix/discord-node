#pragma once

#include <stdio.h>

#define CLAM_DB_HASH "bot.hsb"
#define CLAM_DB_PE "bot.msb"
#define CLAM_DB_URL "url_blacklist.ndb"

void sig_add(const char *sig, const char *db);
char *sig_gen_sha256(FILE *f);
char *sig_gen_url(const char *str);