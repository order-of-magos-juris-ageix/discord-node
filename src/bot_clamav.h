#pragma once

#include <clamav.h>

typedef struct _clamav_ctx {
  unsigned int sigs;
  struct cl_engine *engine;
} clamav_ctx;

void clamav_init();
clamav_ctx *clamav_ctx_init();
void clamav_ctx_free(clamav_ctx *ctx);
cl_error_t clamav_scan(clamav_ctx *ctx, int fd, const char **virname);