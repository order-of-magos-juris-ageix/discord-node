#include "bot_scanner.h"

#include "bot_clamav.h"
#include "bot_sigtool.h"
#include "bot_utils.h"

#include <error.h>
#include <string.h>

#include <fcntl.h>
#include <orca/discord.h>
#include <sys/stat.h>
#include <sys/types.h>

#ifndef O_BINARY
#define O_BINARY 0
#endif

clamav_ctx *clam_ctx;

void setup_clam_ctx() {
  // Init ClamAV context for scan
  clam_ctx = clamav_ctx_init();
  cl_countsigs(cl_retdbdir(), CL_COUNTSIGS_ALL, &clam_ctx->sigs);
  log_info("Signatures Loaded: %d", clam_ctx->sigs);
}

void cleanup_clam_ctx() { clamav_ctx_free(clam_ctx); }

int count_clam_sigs() { return clam_ctx->sigs; }

/**
 * @brief Scan message attachment for potential malware
 *
 * @param scan_ctx Pointer to configured Scan Context (scanner_ctx_t)
 */
void *scan(void *ptr) {
  FILE *f;
  char path[64];

  scanner_ctx_t *scan_ctx = (scanner_ctx_t *)ptr;
  scan_result_t *scan_res = malloc(sizeof(scan_result_t));
  scan_res->result = 0;

  f = get_file(scan_ctx->url, scan_ctx->filename, scan_ctx->id, path, NULL);
  fclose(f);

  scan_ctx->fd = safe_open(path, O_RDONLY | O_BINARY);

  const char *str;
  if (scan_res->result =
          clamav_scan(clam_ctx, scan_ctx->fd, &str) == CL_VIRUS) {
    strncpy(scan_res->virname, str, 64);
    log_warn("Potential Malware Detected: %s | %s", &scan_res->virname, path);

  } else if (scan_res->result != CL_CLEAN) {
    log_error(cl_strerror(scan_res->result));
  }

  // Close and delete downloaded file
  remove(path);
  return (void *)scan_res;
}

scan_result_t *scan_msg(unsigned int id, const char *msg) {
  FILE *f;
  const char *str;
  int fd;
  char path[64];
  scan_result_t *scan_res = malloc(sizeof(scan_result_t));

  f = write_msg_file(id, msg, path);
  fclose(f);

  fd = safe_open(path, O_RDONLY | O_BINARY);

  scan_res->result = clamav_scan(clam_ctx, fd, &str);
  if (scan_res->result == CL_VIRUS) {
    strncpy(scan_res->virname, str, 64);
    log_warn("Blacklisted URL Detected: %s | Message ID: %llu",
             &scan_res->virname, id);
  } else if (scan_res->result != CL_CLEAN) {
    log_error(cl_strerror(scan_res->result));
  }

  // Close and delete downloaded file
  remove(path);
  return scan_res;
}
