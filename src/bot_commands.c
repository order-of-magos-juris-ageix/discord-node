#include "bot_commands.h"

#include "bot_scanner.h"
#include "bot_sigtool.h"
#include "bot_utils.h"

#include <stdlib.h>
#include <string.h>

void cmd_sig_sha256(struct discord *client, const struct discord_message *msg) {
  FILE *f;
  char *hash;
  unsigned int size;
  char filename[64], path[64];
  char desc[256], sig[256], url[256];

  if (auth(msg->author->id))
    return;

  get_attachment(msg, url, filename);
  if (!url || !filename) {
    struct discord_create_message_params params = {
        .content = "No Attachment to Generate SHA256 Sig"};
    discord_create_message(client, msg->channel_id, &params, NULL);
    return;
  }

  if (strcmp(msg->content, "") == 0) {
    struct discord_create_message_params params = {
        .content = "No Signature ID to Generate SHA256 Sig"};
    discord_create_message(client, msg->channel_id, &params, NULL);
    return;
  }

  f = get_file(url, filename, msg->id, path, &size);

  hash = sig_gen_sha256(f);
  if (!hash)
    return;

  snprintf(desc, 256, "File: %s\nHash: %s\nID: %s", filename, hash,
           msg->content);
  struct discord_embed embed = {
      .title = "SHA256 Signature",
      .description = desc,
      .color = 0xFF0000,
      .timestamp = discord_timestamp(client),
  };

  struct discord_create_message_params params = {.embed = &embed};
  discord_create_message(client, msg->channel_id, &params, NULL);

  snprintf(sig, 256, "%s:%d:%s\n", hash, size, msg->content);
  sig_add(sig, CLAM_DB_HASH);

  fclose(f);
  remove(path);
}

void cmd_sig_url(struct discord *client, const struct discord_message *msg) {
  char *sig;
  char path[64];

  if (auth(msg->author->id))
    return;

  if (strcmp(msg->content, "") == 0) {
    struct discord_create_message_params params = {
        .content = "No Signature ID to Generate PDB Sig"};
    discord_create_message(client, msg->channel_id, &params, NULL);
    return;
  }

  struct discord_embed embed = {
      .title = "URL Blacklisted",
      .description = msg->content,
      .color = 0xFF0000,
      .timestamp = discord_timestamp(client),
  };

  struct discord_create_message_params params = {.embed = &embed};
  discord_create_message(client, msg->channel_id, &params, NULL);

  sig = sig_gen_url(msg->content);
  sig_add(sig, CLAM_DB_URL);

  log_info("URL Signature: %s", sig);
}

void cmd_clam_reload(struct discord *client,
                     const struct discord_message *msg) {
  if (auth(msg->author->id))
    return;

  log_info("Realoading ClamAV Database...");

  cleanup_clam_ctx();
  setup_clam_ctx();
}