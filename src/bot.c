#include "bot.h"
#include "auth.h"
#include "bot_clamav.h"
#include "bot_commands.h"
#include "bot_scanner.h"
#include "config.h"

#include <pthread.h>
#include <stdio.h>
#include <string.h>

#include <orca/discord.h>

void on_malware_detected(struct discord *client, const char *filename,
                         u64_snowflake_t channel_id, u64_snowflake_t message_id,
                         const char *virname, struct discord_user *user) {
  // Send embed with detection details
  char desc[384];
  snprintf(desc, 384, "ID: %s\nFile: %s\nUser: %s\nUser ID: %llu", virname,
           filename, user->username, user->id);

  struct discord_embed embed = {
      .title = "TECH HERESY DETECTED",
      .description = desc,
      .color = 0xFF0000,
      .timestamp = discord_timestamp(client),
      .footer = &(struct discord_embed_footer){
          .text = "Damned be the man who reveres not the Omnissiah, your "
                  "ignorance shall be your doom",
          .icon_url = "http://pre05.deviantart.net/0649/th/pre/i/2012/298/2/"
                      "a/adeptus_mechanicus_by_stgene-d5ixfto.png"}};

  struct discord_create_message_params params = {.embed = &embed};
  discord_create_message(client, channel_id, &params, NULL);
}

void on_blacklist_detected(struct discord *client, u64_snowflake_t channel_id,
                           u64_snowflake_t message_id, const char *virname,
                           struct discord_user *user) {
  // Send embed with detection details
  char desc[384];
  snprintf(desc, 384, "ID: %s\nUser: %s\nUser ID: %llu", virname,
           user->username, user->id);

  struct discord_embed embed = {
      .title = "BLACKLISTED URL DETECTED",
      .description = desc,
      .color = 0xFF0000,
      .timestamp = discord_timestamp(client),
      .footer = &(struct discord_embed_footer){
          .text = "From the mouths of heretics emit naught but foul vapours. "
                  "Cut out the tongue and be clean. Quaestos Mechanicor 5.21",
          .icon_url = "http://pre05.deviantart.net/0649/th/pre/i/2012/298/2/"
                      "a/adeptus_mechanicus_by_stgene-d5ixfto.png"}};

  struct discord_create_message_params params = {.embed = &embed};
  discord_create_message(client, channel_id, &params, NULL);
}

void on_ready(struct discord *client) {
  char str[64];
  snprintf(str, 64, "Malware Signatures: %d", count_clam_sigs());
  const struct discord_user *bot = discord_get_self(client);
  struct discord_presence_status presence = {
      .activities =
          (struct discord_activity *[]){
              &(struct discord_activity){.name = str,
                                         .type = DISCORD_ACTIVITY_GAME},
              NULL},
      .status = "Mechanicus Anti-Malware Service",
      .since = discord_timestamp(client)};

  discord_set_presence(client, &presence);
  log_info("Logged on as %s", bot->username);
}

void on_message(struct discord *client, const struct discord_message *msg) {
  if (msg->author->id != discord_get_self(client)->id) {
    if (msg->attachments != NULL) {
      int count = 0;
      for (int i = 0; msg->attachments[i] != NULL; i++) {
        count++;
      }

      int flag = 0;
      scanner_ctx_t scan_ctx[count];
      scan_result_t *scan_res[count];
      pthread_t threads[count];

      for (int i = 0; i < count; i++) {
        strncpy(scan_ctx[i].filename, msg->attachments[i]->filename, 256);
        strncpy(scan_ctx[i].url, msg->attachments[i]->url, 256);

        scan_ctx[i].id = msg->attachments[i]->id;

        // pthread_create(&threads[i], NULL, *scan, (void *)&scan_ctx[i]);
        log_info("Scanning: %s", msg->attachments[i]->filename);
        scan_res[i] = scan((void *)&scan_ctx[i]);
      }

      for (int i = 0; i < count; i++) {
        // pthread_join(threads[i], (void **)&scan_res[i]);
        if (scan_res[i]->result == 1) {
          struct discord_user user;
          discord_get_user(client, msg->author->id, &user);
          on_malware_detected(client, msg->attachments[i]->filename,
                              msg->channel_id, msg->id, scan_res[i]->virname,
                              &user);
          flag = 1;
        }
      }

      if (flag == 1) {
        // Delete message with the detected malware
        discord_delete_message(client, msg->channel_id, msg->id);
      }
    }

    if (strcmp(msg->content, "") != 0) {
      scan_result_t *scan_res = scan_msg(msg->id, msg->content);
      if (scan_res->result == 1) {
        struct discord_user user;
        discord_get_user(client, msg->author->id, &user);
        on_blacklist_detected(client, msg->channel_id, msg->id,
                              scan_res->virname, &user);
        discord_delete_message(client, msg->channel_id, msg->id);
      }
    }
  }
}

void bot_init() {
  struct discord *client = discord_init(AUTH_TOKEN);
  discord_set_prefix(client, COMMAND_PREFIX);

  discord_set_on_ready(client, &on_ready);
  discord_set_on_command(client, "sha256", &cmd_sig_sha256);
  discord_set_on_command(client, "url", &cmd_sig_url);
  discord_set_on_command(client, "reload", &cmd_clam_reload);
  discord_set_on_message_create(client, &on_message);

  log_info("Initializing ClamAV...");
  clamav_init();
  setup_clam_ctx();

  log_info("Connecting to Discord...");

  while (true) {
    ORCAcode ret = discord_run(client);
    log_error("Discord Client exited: %d | Restarting...", ret);
  }

  cleanup_clam_ctx();
  discord_cleanup(client);
}