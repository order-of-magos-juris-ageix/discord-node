#include "bot_utils.h"

#include "auth.h"

#include <string.h>

#include <curl/curl.h>
#include <sys/stat.h>
#include <sys/types.h>

size_t download_write(void *ptr, size_t size, size_t nmemb, FILE *stream) {
  return fwrite(ptr, size, nmemb, stream);
}

/**
 * @brief Download File for URL
 *
 * @param url URL to the File to download
 * @param file File Descriptor
 */
void download(const char *url, FILE *file) {
  CURL *curl = curl_easy_init();
  curl_easy_setopt(curl, CURLOPT_URL, url);
  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, download_write);
  curl_easy_setopt(curl, CURLOPT_WRITEDATA, file);

  curl_easy_perform(curl);
  curl_easy_cleanup(curl);
}

int auth(unsigned int id) { return id == TRUSTED_USER_ID ? 1 : 0; }

void get_attachment(const struct discord_message *msg, char *url,
                    char *filename) {
  if (msg->referenced_message) {
    if (msg->referenced_message->attachments != NULL) {
      strncpy(url, msg->referenced_message->attachments[0]->url, 256);
      strncpy(filename, msg->referenced_message->attachments[0]->filename, 64);
      return;
    }
  } else if (msg->attachments) {
    strncpy(url, msg->attachments[0]->url, 256);
    strncpy(filename, msg->attachments[0]->filename, 64);
    return;
  }

  return;
}

FILE *get_file(const char *url, const char *filename, unsigned int id,
               char *path, unsigned int *size) {
  register FILE *f;

  // Setup and download the file from the attachment
  mkdir("tmp", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  snprintf(path, 64, "tmp/%s-%0X", filename, id);
  f = fopen(path, "wb+");

  download(url, f);

  if (size != NULL) {
    *size = ftell(f);
  }

  fseek(f, 0, SEEK_SET);

  return (f);
}

FILE *write_msg_file(unsigned int id, const char *str, char *path) {
  register FILE *f;

  mkdir("tmp", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  snprintf(path, 64, "tmp/%0X.msg", id);
  f = fopen(path, "w+");

  fputs(str, f);

  return f;
}