TARGET    = bot
SRCDIR    = src
CC        = gcc
CCFLAGS   = -g -std=c17 
LDFLAGS   = -pthread -lclamav -ldiscord -lcurl
SRCS      = $(foreach dir,$(SRCDIR), $(wildcard $(dir)/*.c))
OBJS      = $(SRCS:.c=.o)

%.o: %.c ; $(CC) -c $(CCFLAGS) $< -o $@

release: $(TARGET)

$(TARGET): $(OBJS)
	$(CC) $^ $(CCFLAGS) $(LDFLAGS) -o $@

clean:
	rm $(TARGET) $(OBJS)
